import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
  import {getDatabase, onValue, ref,set,child,get,update,remove} from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";
  import {getStorage, ref as refS, uploadBytes, getDownloadURL } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-storage.js";
  import{getAuth, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js";
  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries

  // Your web app's Firebase configuration
  const firebaseConfig = {
    apiKey: "AIzaSyAJNyBzApO4uV0dtjbfWR3wokd7MSHGzos",
    authDomain: "misitioweb-575df.firebaseapp.com",
    projectId: "misitioweb-575df",
    //databaseURL:"https://misitioweb-575df-default-rtdb.firebaseio.com/",
    storageBucket: "misitioweb-575df.appspot.com",
    messagingSenderId: "898417424270",
    appId: "1:898417424270:web:bd767d963dd63fe1a616df"
  };



  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
  const db= getDatabase();

  //pagina admin
  var btnAgregar=document.getElementById('btnAgregar');
  var btnBorrar=document.getElementById('btnBorrar');
  var btnActualizar= document.getElementById('btnActualizar');
  var btnMostrar= document.getElementById('btnMostrar');
  var btnBuscar= document.getElementById('btnBuscar');
  var Dis= document.getElementById('DC');
  var archivo= document.getElementById('archivo');
  var verImagen= document.getElementById('verImagen');

  var cursos=document.getElementById('a');
  var id="";
  var Nombre= "";
  var des="";
  var precio="";
  var status="";
  var nombreimg="";
  var url="";
  
  function leer(){
    id=document.getElementById('ID').value;
    Nombre=document.getElementById('nombre').value;
    des=document.getElementById('des').value;
    precio=document.getElementById('precio').value;
    status=document.getElementById('status').value;
    nombreimg= document.getElementById('imgNombre').value;
    url= document.getElementById('URL').value;
  }

  function escribirinputs(){
    document.getElementById('ID').value=id;
    document.getElementById('nombre').value=Nombre;
    document.getElementById('des').value= des;
    document.getElementById('precio').value=precio;
    document.getElementById('status').value=status;
    document.getElementById('imgNombre').value=nombreimg;
    document.getElementById('URL').value=url;
  }

  function insertar(){
    leer();
    set(ref(db,"cursos/" + id),{
   nombre:Nombre,
    descripcion:des,
    precio:precio,
    Status:status,
    nombreimg: nombreimg,
    url: url

    }).then((docRef)=>{
        alert("Se agrego el registro con exito");
        mostrarDatos();
        limpiar();
    
    }).catch((error)=>{
        alert("Surgio un error", error);
    })

  };

  function mostrarDatos(){
    leer();
    const dbref= ref(db);
    cursos.innerHTML="";
    get(child(dbref,'cursos/' + id)).then((snapshot)=>{
        if(snapshot.exists()){
            Nombre= snapshot.val().nombre;
            des = snapshot.val().descripcion;
            status= snapshot.val().Status;
            precio=snapshot.val().precio;
            nombreimg= snapshot.val().nombreimg;
            url= snapshot.val().url;
            if(status=="Disponible"){
              escribirinputs();
              
              cursos.innerHTML= cursos.innerHTML +"<div id='cur'>" + id +     
              "<center>" + 
              "<img src='"+url+"' alt=''>" +
              "<h3>" + "Curso de " + Nombre + "</h3>" + 
              "<h4>Contiene:</h4>" +
              "<li>" + des + "</li><br>" +
              "<a href=''>$" + precio +"MXN</a>"+ "<br>"+
              "<button id='btn2' >Disponible</button>"+
              "<br>" +
              "</center></div>" ;
              }else{
                limpiar();
              }
        }else{
            alert("No existe la id");
        }
    }).catch((error)=>{
        alert("Surgio un error" + error);
    })
  }

  function actualizar(){
    leer();
    update(ref(db, 'cursos/' + id),{
      nombre:Nombre,
      descripcion:des,
      precio:precio,
      Status:status,
      nombreimg: nombreimg,
      url: url
    }).then(()=>{
        alert("Se realizó actualizacion");
        mostrarDatos();
        limpiar();
    }).catch(() => {
        alert("Surgio un error" + error);
        limpiar();
    });
  }

  function deshabilitar(){
    leer();
    update(ref(db, 'cursos/' + id),{
      Status:"No disponible"
    }).then(()=>{
        alert("Se deshabilitó");
        mostrarDatos();
        limpiar();
    }).catch(() => {
        alert("Surgio un error" + error);
    });
  }

  function limpiar(){
    id="";
    Nombre="";
    status="";
    precio="";
    des="";
    nombreimg="";
    url="";
    escribirinputs();
  }
/*
  function mostrarCursos(){
    const db = getDatabase();
    const dbRef= ref(db, 'cursos');

    onValue(dbRef,(snapshot) => {
        cursos.innerHTML="";
        snapshot.forEach((childSnapshot) => {
         
            
            const childData = childSnapshot.val();
            if(childData.Status=="Disponible"){
            cursos.innerHTML= cursos.innerHTML +"<div id='cur'>" +   
            "<center>" + 
            "<img src='"+childData.url+"' alt=''>" +
            "<h3>" + "Curso de " + childData.nombre + "</h3>" + 
            "<h4>Contiene:</h4>" +
            "<li>" + childData.descripcion + "</li><br>" +
            "<a href=''>$" + childData.precio +"MXN</a>"+ "<br>"+
            "<button id='btn2' >Disponible</button>"+
            "<br>" +
            "</center></div>" ;
            }else if(childData.Status=="No disponible"){
              cursos.innerHTML= cursos.innerHTML +"<div id='cur2'>" +
            "<center>" + 
            "<img src='"+childData.url+"' alt=''>" +
            "<h3>" + "Curso de " + childData.nombre + "</h3>" + 
            "<h4>Contiene:</h4>" +
            "<li>" + childData.descripcion + "</li><br>" +
            "<a href=''>$" + childData.precio +"MXN</a>"+ "<br>"+
            "<button id='btn2' >No Disponible</button>"+
            "<br>" +
            "</center></div>" ;
            }
        });
        {
            onlyOnce: true
        }
    });
    
  }
  */
  function cargarImagen(){
    const file= event.target.files[0];
    const name= event.target.files[0].name;
  
    const storage= getStorage();
    const storageRef= refS(storage, 'cursos/' + name);
  
    uploadBytes(storageRef, file).then((snapshot) => {
      document.getElementById('imgNombre').value=name;
  
      alert('se cargo la imagen');
    });
  }
  
  function descargarImagen(){
    archivo= document.getElementById('imgNombre').value;
    // Create a reference to the file we want to download
  const storage = getStorage();
  const starsRef = refS(storage, 'cursos/' + archivo);
  
  // Get the download URL
  getDownloadURL(starsRef)
    .then((url) => {
     document.getElementById('URL').value=url;
     document.getElementById('imagen').src=url;
    })
    .catch((error) => {
      // A full list of error codes is available at
      // https://firebase.google.com/docs/storage/web/handle-errors
      switch (error.code) {
        case 'storage/object-not-found':
          console.log("No existe el archivo");
          break;
        case 'storage/unauthorized':
          console.log("No tiene permisos");
          break;
        case 'storage/canceled':
          console.log("No existe conexion con la base de datos")
          break;
  
        // ...
  
        case 'storage/unknown':
          console.log("Ocurrio algo inesperado")
          break;
      }
    });
  }
  
  btnAgregar.addEventListener('click', insertar);
  btnBuscar.addEventListener('click', mostrarDatos);
  //window.onload(mostrarCursos());
  btnActualizar.addEventListener('click', actualizar);
  btnBorrar.addEventListener('click', deshabilitar);
  archivo.addEventListener('change', cargarImagen);
  verImagen.addEventListener('click', descargarImagen);
   
