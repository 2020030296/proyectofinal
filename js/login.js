import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
    import{getAuth, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js";
  const firebaseConfig = {
    apiKey: "AIzaSyAJNyBzApO4uV0dtjbfWR3wokd7MSHGzos",
    authDomain: "misitioweb-575df.firebaseapp.com",
    projectId: "misitioweb-575df",
    //databaseURL:"https://misitioweb-575df-default-rtdb.firebaseio.com/",
    storageBucket: "misitioweb-575df.appspot.com",
    messagingSenderId: "898417424270",
    appId: "1:898417424270:web:bd767d963dd63fe1a616df"
  };
  const app = initializeApp(firebaseConfig);


  var btnLogin= document.getElementById('btnLogin');
  var btnLimpiar= document.getElementById('btnLimpiar');
  
  function login(){
    let email = document.getElementById('Usuario').value;
    let contra= document.getElementById('contra').value;

    const auth = getAuth();
    signInWithEmailAndPassword(auth, email, contra)
    .then((userCredential) => {
    // Signed in 
    const user = userCredential.user;
    location.href="/html/admin.html";
     })
    .catch((error) => {
    location.href="/html/error.html";
    });
  }

  function limpiar(){
    document.getElementById('Usuario').value="";
    document.getElementById('contra').value="";
  }

  btnLogin.addEventListener('click', login);
  btnLimpiar.addEventListener('click', limpiar);